/**
 * 开放平台模式,配置文件不需要修改
 * 非开放平台应用可以删除ext.js直接在这里修改对应配置
 */
let extConfig = wx.getExtConfigSync ? wx.getExtConfigSync() : {}
module.exports = {
    name: extConfig.name,
    siteroot: extConfig.attr.host,
    api_id: extConfig.attr.api_id,
    api_secret: extConfig.attr.api_secret
};