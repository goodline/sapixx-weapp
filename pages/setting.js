const app = getApp()
Page({

  //清空缓存
  clearStorage: function () {
    app.wxLayer('请确认要退出登录?',() => {
      try {
        wx.clearStorageSync()
        wx.reLaunch({
          url: '../tab/index',
        })
      } catch(e) {
        app.wxAlert('请退出登录');
      }
    })
  }
})