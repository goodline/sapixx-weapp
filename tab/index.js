const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    skeleton:true,
    loginstate:false
  },
  //监听页面初次载入完成
  onLoad: function (options) {
    app.getParam(options);
  },
  //加载完成
  onReady(){
    this.setData({
      skeleton:false
    })
  },
  //生命周期函数--监听页面显示
  onShow: function () {
    app.loginState((state)=>{
      if(state){
        this.setData({
          loginstate:state
        })

      }
    })
  },
  //登录状态
  onLoginState: function (e) {
    if(e.detail.state == true){
      this.setData({
        loginstate:true,
        ucode:app.globalData.loginuser.ucode,
      })
    }
  },
  //登录
  onLogin: function () {
    this.setData({
      loginstate:false,
    })
  },
  //链接
  onUrl:function (e) {
    if(this.data.loginstate){
      wx.navigateTo({
        url:e.currentTarget.dataset.url
      })
    }else{
      this.onLogin();
    }
  },
  //滚动
  onPageScroll(res){
    let scrollTop = res.scrollTop;
    this.setData({
      showbar:10 <= scrollTop ? true : false
    })
  }
})