# SAPI++ 小程序开发基础实例文件

#### 介绍
SAPI++多应用多租户SaaS应用快速开发框架(小程序端开发实例)


#### 注意事项


1. 配置文件 ext.json 基于微信开放平台的(如不是微信开放平台可以删除)
1. 如删除了ext.json,请把ext.json的配置手动配置到config.js
1. API签名请求封装 utils/request.js 已封装好签名规则
