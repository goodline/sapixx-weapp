const app = getApp()

Component({

  properties: {

    loginstate: {
      type: Boolean,
      value: false
    },
    
    loading: {
      type: Boolean,
      value: false
    },

  },
  attached: function () {
    const systemInfo = wx.getSystemInfoSync();
    this.setData({
      systemInfo: {
        width: systemInfo.windowWidth,
        height: systemInfo.windowHeight
      },
    })
  },
  ready: function () {

  },
  methods: {
    //隐藏
    onHideOrder() {
      this.setData({
        loginstate:true
      });
    },
    //用户登录
    authorLogin: function (options) {
      var that = this;
      if (options.detail.errMsg !== 'getUserInfo:ok') {
        return false;
      }
      that.setData({
        loading:true
      })
      wx.login({
        success: function (res) {
          app.api().Post('apis/service/login/weapp',{
            code: res.code,
            invite: '0004'
          },(result) => {
            wx.setStorageSync('token',result.data.token);
            wx.setStorageSync('session_id',result.data.session_id);
            wx.setStorageSync('loginuser', result.data);
            app.globalData.loginuser = result.data;
            that.setData({
              loading:false
            })
            that.triggerEvent('onState',{state:true} ,{})
            that.onHideOrder()
          })
        },
        fail: function () {
          that.setData({
            loading:false
          })
          app.wxAlert('授权失败,请重试.')
        }
      });
    },
    //锁屏
    touchmove: function () {

    }
  },
})