Component({

  properties: {

    show: {
      type: Boolean,
      value: true
    },
    
    loading: {
      type: Boolean,
      value: false
    },

  },
  attached: function () {
    const systemInfo = wx.getSystemInfoSync();
    this.setData({
      systemInfo: {
        width: systemInfo.windowWidth,
        height: systemInfo.windowHeight
      },
    })
  },
  ready: function () {
    if(this.properties.loading == false){
      setTimeout(() => {
        this.setData({
          show: false
        })
      },1000)
    }
  },
  methods: {
    //锁屏
    touchmove: function () {

    }
  },
})